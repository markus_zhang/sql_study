﻿/*
Get the laptop models that have a speed smaller than the speed of any PC. 
Result set: type, model, speed.
http://www.sql-ex.ru/exercises/index.php?act=learn&LN=17#resPlace
*/


SELECT
    DISTINCT
    P.type,
    L.model,
    L.speed
FROM
    Laptop AS L INNER JOIN Product AS P ON L.model = P.model
WHERE
    L.speed < (
        SELECT
            MIN(P.speed)
        FROM
            PC AS P
        WHERE P.speed IS NOT NULL
    ) AND L.speed IS NOT NULL