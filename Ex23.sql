﻿/*
Get the makers producing both PCs having a speed of 750 MHz or higher and laptops with a speed of 750 MHz or higher. 
*/


SELECT
    DISTINCT ZZ.maker
FROM
(
	SELECT
		Z.maker,
		COUNT(Z.PC_model) AS PC_Count,
		COUNT(Z.Laptop_model) AS Laptop_Count
	FROM
	(
		SELECT
			P.maker,
			PC.model AS PC_model,
			L.model AS Laptop_model
		FROM
			Product AS P
			LEFT JOIN PC ON P.model = PC.model AND PC.speed >= 750
			LEFT JOIN Laptop AS L ON P.model = L.model AND L.speed >= 750
		WHERE
			P.type != 'Printer'
	) AS Z
	GROUP BY Z.maker
	HAVING (COUNT(Z.PC_model) >= 1 AND COUNT(Z.Laptop_model) >= 1)
) AS ZZ