﻿--1. Average daily revenue and Average daily number of transactions per VIP player
--Using T-SQL
--Assuming 2018-10

SELECT
	V.user_id AS vip_id,
	SUM(ISNULL(TRANX.revenue, 0)) / 31 AS avg_revenue,	--31 days for October
	COUNT(TRANX.nb_transactions) / 31 AS avg_num_tranx
FROM
	VIP_list AS V
	LEFT JOIN transactions AS TRANX ON V.user_id = TRANX.user_id
WHERE
	1=1
	AND TRANX.calendar >= '2018-10-01'
	AND TRANX.calendar < '2018-11-01'
GROUP BY
	V.user_id
--Let's say you want to order them by average revenue
ORDER BY
	SUM(ISNULL(TRANX.revenue, 0)) / 31 DESC;

--2.Build a list of Android users who haven't played for 30 days, needs user_id, email and facebook_id and flag for tier (1,2,3 against 4,5,6,7)
--Explanation:
--1) For tiers, the flag shows 1 for tier 1,2,3 and 2 for 4,5,6,7 (integer used for efficiency), 0 is used for NULL tier
--2) In case last_played is NULL, take install_date as date last played (we can wrap another ISNULL() if install_date is also NULL, doesn't show here)

SELECT
	U.user_id,
	U.email,
	U.facebook_id,
	CASE 
		WHEN U.tiers IS NULL THEN 0
		WHEN U.tiers <= 3 THEN 1 
		ELSE 2
	END AS flag
FROM
	user_profile AS U
WHERE
	1=1
	AND	U.platform = 'Android'
	AND DATEDIFF(day, CAST(ISNULL(U.last_played, U.install_date) AS date), GETDATE()) > 30

--3.Assess the upllift over the same period in the previous week, split by tiers and payment source
--Date could be fixed in the query, but apparently it's more flexible if we use two variables for the datetimes and one variable for the difference of days
--and use a DATEADD() to grab the control datetimes
--ISNULL sets the aggregated metrics to 0 instead of NULL

DECLARE @PromoBeginDatetime datetime;
DECLARE @PromoEndDatetime datetime;
DECLARE @DayDiff int;

SET @PromoBeginDatetime = '2018-11-23 16:00:00';
SET @PromoEndDatetime = '2018-11-23 16:00:00';
SET @DateDiff = -7;

SELECT
	Mtemp.tiers,
	Mtemp.payment_source,
	Mtemp.promo_revenue - Mtemp.control_revenue AS uplift_revenue,
	Mtemp.promo_num_tranx - Mtemp.control_num_tranx AS uplift_num_tranx
FROM (
	SELECT
		M.tiers,
		M.payment_source,
		ISNULL(SUM(CASE 
				WHEN M.event_ts BETWEEN @PromoBeginDatetime AND @PromoEndDatetime THEN ISNULL(M.revenue, 0)
			END), 0) AS promo_revenue,
		ISNULL(SUM(CASE
				WHEN M.event_ts BETWEEN DATEADD(day, @DateDiff, @PromoBeginDatetime) AND DATEADD(day, @DateDiff, @PromoEndDatetime) THEN ISNULL(M.revenue, 0)
			END), 0) AS control_revenue,
		ISNULL(COUNT(CASE 
				WHEN M.event_ts BETWEEN @PromoBeginDatetime AND @PromoEndDatetime THEN M.event_ts
			END), 0) AS promo_num_tranx,
		ISNULL(COUNT(CASE 
				WHEN M.event_ts BETWEEN DATEADD(day, @DateDiff, @PromoBeginDatetime) AND DATEADD(day, @DateDiff, @PromoEndDatetime) THEN M.event_ts
			END), 0) AS control_num_tranx
	FROM
		monetization AS M
	GROUP BY
		M.tiers, M.payment_source
) AS Mtemp