﻿/*	Nobel database practices
	nobel(yr, subject, winner)  
*/

--	9. Show the years in which three prizes were given for Physics

--	Solution
SELECT nobel.yr
FROM nobel
WHERE nobel.subject = 'Physics'
GROUP BY nobel.yr
HAVING COUNT(nobel.yr) = 3
--	End of Solution

--	Notes
--	You want to SELECT on nobel.yr too show the years
--	Limit the subject to 'Physics'
--	Then you want to count the number of prizes with HAVING COUNT()
--	The trick part is that we need to count the years to count the number prizes
--	End of Notes


--	10. Show winners who have won more than once.

--	Solution
SELECT nobel.winner
FROM nobel
GROUP BY nobel.winner
HAVING COUNT(nobel.winner) > 1
--	End of Solution

--	11. Show winners who have won more than one subject.

--	Solution
SELECT nobel.winner
FROM nobel
GROUP BY nobel.winner
HAVING COUNT(DISTINCT nobel.subject) >= 2
--	End of Solution

--	Notes
--	The tricky part is to realize that one winner may win multiple times for the same subject
--	So we need to use DISTINCT in COUNT()
--	End of Notes

--	12. Show the year and subject where 3 prizes were given. Show only years 2000 onwards.

--	Solution
SELECT nobel.yr, nobel.subject
FROM nobel
WHERE nobel.yr >= 2000
GROUP BY nobel.yr, nobel.subject
HAVING COUNT(nobel.subject) = 3
--	End of Solution

--	Notes
--	WHERE must be put before GROUP BY
--	GROUP BY must contain all fields in SELECT otherwise runtime error
--	Use HAVING instead of WHERE for aggregating functions as limitation
--	End of Notes

/*	World database practices
	world(name, continent, area, population, gdp) 
*/

-- 6. Which countries have a GDP greater than every country in Europe? [Give the name only.] 
-- (Some countries may have NULL gdp values)

-- Solution:
SELECT world.name
FROM world
WHERE world.GDP > ALL(SELECT world.GDP
                      FROM world
                      WHERE world.continent = 'Europe'
                      AND world.GDP IS NOT NULL)
-- End of Solution

-- Notes
-- AND world.GDP IS NOT NULL can be replaced with AND world.GRP > 0
-- will return nothing if this line is missing
-- End of Notes

-- 8. List each continent and the name of the country that comes first alphabetically.

-- Solution:
SELECT world.continent, world.name
FROM world
WHERE world.name = (SELECT MIN(x.name)
                    FROM world AS x
                    WHERE x.continent = world.continent)
-- End of Solution

-- Notes
-- In the sub query, needs to use WHERE x.continent = world.continent to match continent of two tables (x and world)
-- If we don't have the WHERE in sub query, MIN(x.name) will only return the smallest country in the whole table (i.e. Afganistan)
-- End of Notes

-- 9. Find the continents where all countries have a population <= 25000000. 
--	  Then find the names of the countries associated with these continents. Show name, continent and population.

-- Solution:
SELECT world.name, world.continent, world.population
FROM world
WHERE world.continent NOT IN (SELECT world.continent
                              FROM world
                              WHERE world.population > 25000000)
-- End of Solution

-- Notes
-- The tricky is to use the sub query to exclude continents with even just one exception
-- The sub query just returns a list of repeated continents if the correspodent country has >25,000,000 population:
/*
continent
Asia
Africa
South America
Asia
South America
North America
Asia
South America
Africa
Asia
Africa
Europe
Europe
Africa
Asia
Asia
Asia
Asia
Europe
Asia
Africa
Asia
North America
*/
-- We don't care if it's duplicated, because we use this list to filter any row that has its continent in it
-- End of Notes

-- 10. Some countries have populations more than three times that of any of their neighbours (in the same continent). 
--     Give the countries and continents.

-- Solution:
SELECT world.name, world.continent
FROM world
WHERE world.population > 3 * (SELECT MAX(x.population)
                              FROM world AS x
                              WHERE x.continent = world.continent
                                AND x.name <> world.name)
-- End of Solution

-- Notes
-- In the sub query, needs to use MAX teamed with x.name <> world.name to get the maximum population other countries of the same continent have
-- End of Notes
