﻿SELECT
	*
	--P.id,
	--SUM(C.SCORE) AS Comment_Score
FROM
	STACKOVERFLOW.PUBLIC.POSTS AS P
	LEFT JOIN STACKOVERFLOW.PUBLIC."comments" AS C
		ON P.ID = C.POSTID
WHERE P.TAGS LIKE '%<ai>%'
--GROUP BY P.ID

SELECT 
	P.ID,
	COUNT(V.ID)
FROM 
	POSTS AS P
	LEFT JOIN VOTES AS V ON P.ID = V.POSTID


SELECT
	*
FROM
	POSTTYPES AS T 

--Interesting queries:
--1. 	How many upvotes do I have for each tag? (how long before tag badges?)
--2. 	How high is the accepted percentage of my answers?
--3. 	How many votes do my comments have? (how long before Pundit?)
--4. 	How high would my reputation approximately be when there was no cap or CW?
--5. 	Users with more than 10 accounts on same email
--6. 	Downvote ratio of users with > 10 downvotes (the pessimists ;-) )
--7. 	What is my average answer score? (change PostTypeId to 1 to get average question score)
--8. 	Users with highest average answer score (and having > 100 answers)
--9. 	Users with high self-accept rates (and having > 10 answers) (the extreme self-learners)
--10.	Upvote/Downvote ratio per day of week (more risk on downvotes in weekends?)
--11.	How many days was I active? (Returns amount of days when you've posted at least one answer, may be useful for another statistics since registrationdate isn't always representative for "user activity").
--12. 	Most controversial posts on the Site
--13. 	Search Stack Overflow Favorites by Tag Name
--14. 	Users ordered by accepted answer rate

--1. 	How many upvotes do I have for each tag? (how long before tag badges?)
SELECT
	U.DISPLAYNAME,
	T.TAGNAME,
	SUM(P.SCORE) AS totalscore
	--P.TITLE,
	--P.BODY,
	--P.SCORE
FROM
	USERS AS U
	INNER JOIN POSTS AS P ON U.ID = P.OWNERUSERID 
	INNER JOIN POSTTAGS AS PT ON P.PARENTID = PT.POSTID OR P.ID = PT.POSTID
	INNER JOIN TAGS AS T ON PT.TAGID = T.ID
WHERE
	U.DISPLAYNAME = 'bummzack'
GROUP BY T.ID, T.TAGNAME, U.DISPLAYNAME
ORDER BY SUM(P.SCORE) DESC

--2. 	How high is the accepted percentage of my answers?
SELECT
	P.*
FROM
	USERS AS U
	INNER JOIN POSTS AS P ON U.ID = P.OWNERUSERID
	
	--This is wrong, because he probably answers other user's post
	--INNER JOIN POSTS AS P ON U.ID = P.OWNERUSERID AND P.ACCEPTEDANSWERID IS NOT NULL
	--LEFT JOIN POSTS AS P2 ON P.ACCEPTEDANSWERID = P2.ID	--Grabbing only the accepted answers
WHERE
	1=1
	AND U.DISPLAYNAME = 'bummzack'
	AND P.ID IN (
		SELECT P.ACCEPTEDANSWERID FROM POSTS AS P
	)

--3.	Segment comments with score (For score A, you have X comments, for score B, Y comments, etc.)
SELECT
	C.SCORE,
	COUNT(*) AS NumComments
FROM
	"comments" AS C
	LEFT JOIN USERS AS U ON C.USERID = U.ID
WHERE
	U.DISPLAYNAME = 'bummzack'
GROUP BY C.SCORE

--4. 	How high would my reputation approximately be when there was no cap or CW?
--I don't GET it

--5. 	Users with more than 10 accounts on same email
--NO emailhash information...

--6. 	Downvote ratio of users with > 10 downvotes
SELECT
	U.DISPLAYNAME,
	CAST(U.DOWNVOTES AS float) / (CAST((CASE WHEN U.UPVOTES = 0 THEN 1 ELSE U.UPVOTES END) AS float)) AS downvoteratio
FROM
	USERS AS U
WHERE
	1=1
	AND U.DOWNVOTES > 10


--7. 	What is my average answer score? (change PostTypeId to 1 to get average question score)
SELECT
	AVG(P.SCORE)
FROM
	USERS AS U
	INNER JOIN POSTS AS P ON U.ID = P.OWNERUSERID
WHERE
	1=1
	AND U.DISPLAYNAME = 'bummzack'
	AND P.POSTTYPEID = 2		--Answer

--7.1 	What is my average answer score given that my answer is the only one 
SELECT
	AVG(P.SCORE)
	--P2.ANSWERCOUNT,
	--P.*
FROM
	USERS AS U
	INNER JOIN POSTS AS P ON U.ID = P.OWNERUSERID
	INNER JOIN POSTS AS P2 ON P.PARENTID = P2.ID	--This is the important 
WHERE
	1=1
	AND U.DISPLAYNAME = 'bummzack'
	AND P.POSTTYPEID = 2			--Answer
	AND P2.ANSWERCOUNT = 1			--ONLY one ANSWER

--8. 	Users with highest average answer score (and having > 100 answers)
SELECT
	*
FROM (
	SELECT
		U.ID,
		COUNT(P.ID) AS answercount,
		SUM(P.SCORE) AS scoretotal
	FROM
		USERS AS U
		LEFT JOIN POSTS AS P ON U.ID = P.OWNERUSERID
	WHERE
		1=1
		AND P.POSTTYPEID = 2			--Answer
	GROUP BY U.ID
) AS subq
WHERE subq.answercount >= 100
ORDER BY CAST(SUBQ.scoretotal AS float) / CAST(SUBQ.answercount AS float) DESC


--9. 	Users with high self-accept rates (and having > 10 answers)

--Here is a more difficult query: List the topics with their accespted answers, side by side
--Because this query starts from USERS, there are some users who removed themselves from stackexchange so the total count 24,022 is slightly less than 24,286
--For example: https://gamedev.stackexchange.com/questions/1489/how-to-allow-character-to-keep-moving-after-it-hits-a-wall
SELECT
	P.*,
	SUBQ.*
FROM
	USERS AS U
	LEFT JOIN POSTS AS P ON U.ID = P.OWNERUSERID AND P.ACCEPTEDANSWERID IS NOT NULL
	INNER JOIN LATERAL (
		SELECT
			*
		FROM
			POSTS AS P2
		WHERE P2.ID = P.ACCEPTEDANSWERID
	) AS SUBQ ON TRUE
ORDER BY P.ID

--Now IF you still want the answer TO NO.9:
SELECT
	P.*,
	SUBQ.*
FROM
	USERS AS U
	LEFT JOIN POSTS AS P ON U.ID = P.OWNERUSERID AND P.ACCEPTEDANSWERID IS NOT NULL
	INNER JOIN LATERAL (
		SELECT
			*
		FROM
			POSTS AS P2
		WHERE P2.ID = P.ACCEPTEDANSWERID AND P2.OWNERUSERID = P.OWNERUSERID		--Added for 
	) AS SUBQ ON TRUE
ORDER BY P.ID


--10.	Upvote/Downvote ratio per day of week (more risk on downvotes in weekends?)
SELECT
	*
FROM
	VOTES AS V


--11.	How many days was I active? 
SELECT
	PUSER.*
FROM
	USERS AS U
	INNER JOIN (
		SELECT 
			*
		FROM
			POSTS AS P
	) AS PUSER ON U.ID = PUSER.OWNERUSERID
WHERE
	1=1
	AND U.DISPLAYNAME = 'bummzack'
ORDER BY PUSER.CREATIONDATE ASC
LIMIT 1

--12. 	Most controversial posts on the Site
--We will look AT the votes
SELECT
	*
FROM (
	SELECT
		V.POSTID,
		SUM(CASE WHEN VT."name" = 'UpMod' THEN 1 ELSE 0 END) AS upvotes,
		SUM(CASE WHEN VT."name" = 'DownMod' THEN 1 ELSE 0 END) AS downvotes
	FROM
		VOTES AS V
		LEFT JOIN VOTETYPES AS VT ON V.VOTETYPEID = VT.ID
	GROUP BY
		V.POSTID
) AS MRESULT
WHERE
	1=1
	AND (MRESULT.upvotes != 0 OR MRESULT.downvotes != 0)
ORDER BY ABS(MRESULT.upvotes - MRESULT.downvotes) ASC, MRESULT.upvotes + MRESULT.downvotes DESC

--14. 	Users ordered by accepted answer rate
SELECT 
	* 
FROM USERS AS U
	LEFT JOIN POSTS AS P ON U.ID = P.OWNERUSERID
WHERE
	1=1
	AND P.ACCEPTEDANSWERID IS NOT NULL