﻿--Under the assumption that receipts of money (inc) and payouts (out) are registered not more than once a day for each collection point [i.e. the primary key consists of (point, date)], 
--write a query displaying cash flow data (point, date, income, expense). 
--Use Income_o and Outcome_o tables.

SELECT
    I.point,
    I.date,
    I.inc AS income,
    O.out AS expense
FROM
    Income_o AS I
    LEFT JOIN Outcome_o AS O ON (I.point = O.point AND I.date = O.date)
UNION
SELECT
    O.point,
    O.date,
    I.inc AS income,
    O.out AS expense
FROM
    Outcome_o AS O
    LEFT JOIN Income_o AS I ON (I.point = O.point AND I.date = O.date)