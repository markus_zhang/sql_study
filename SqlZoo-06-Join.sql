﻿/*	UEFA Football database practices

*/

--	3. Show the player, teamid, stadium and mdate for every German goal

--	Solution:
SELECT goal.player, goal.teamid, game.stadium, game.mdate
FROM goal JOIN game ON goal.matchid = game.id JOIN eteam ON eteam.id = goal.teamid
WHERE eteam.teamname = 'Germany'
--	End of Solution

--	Notes:
--	A better solution, I think, it's to use sub queries to reduce the number of records to JOIN
SELECT goal.player, goal.teamid, game.stadium, game.mdate
FROM goal 
	JOIN game ON goal.matchid = game.id 
		JOIN 
			(SELECT * FROM eteam WHERE eteam.teamname = 'Germany') AS teamger ON teamger.id = goal.teamid
--	We need to assign a temp name for the sub query result
--	End of Notes

--	8. Show the name of all players who scored a goal against Germany

--	Solution:
SELECT DISTINCT NotGER.player
FROM (SELECT *
		FROM goal
        WHERE goal.teamid <> 'GER') AS NotGER JOIN game on game.id = NotGER.matchid
WHERE game.team1 = 'GER' OR game.team2 = 'GER'
--	End of Solution

--	Notes
--	The sub query is used to limit the range of search because if the player is playing against Germany he surely is not of German team
--	Once we pick up a non-German player, regardless he is team 1 or 2, as long as he has a goal and one of the teams is Germany, then we know it's a goal against Germany
--	End of Notes

--	9. Show teamname and the total number of goals scored.

--	Solution
SELECT eteam.teamname, COUNT(goal.matchid) AS '# of goals'
FROM eteam JOIN goal ON goal.teamid = eteam.id
GROUP BY eteam.teamname
--	End of Solution

--	Notes
--	1) Number of goals, whence we fix a team, is the number of rows in table goal because each row is a goal
--	2) GROUP BY is needed for COUNT() to fix the team
--	End of Notes

--	11. For every match involving 'POL', show the matchid, date and the number of goals scored.

--	Solution:
SELECT goal.matchid, game.mdate, COUNT(goal.teamid) AS '# of goals'
FROM goal JOIN game ON goal.matchid = game.id
WHERE game.team1 = 'POL' OR game.team2 = 'POL'
GROUP BY goal.matchid, game.mdate
--	End of Solution

--	Notes
--	Need to GROUP BY both columns, and POL can appear on both sides
--	End of Notes

--	13. List every match with the goals scored by each team as shown
--	mdate			team1	score1	team2	score2
--	1 July 2012		ESP		4		ITA		0
--	10 June 2012	ESP		1		ITA		1
--	10 June 2012	IRL		1		CRO		3

--	Solution:
SELECT game.mdate,
       game.team1,
       SUM(CASE WHEN game.team1 = goal.teamid THEN 1 ELSE 0 END) AS score1,
       game.team2,
       SUM(CASE WHEN game.team2 = goal.teamid THEN 1 ELSE 0 END) AS score1
FROM game LEFT OUTER JOIN goal ON game.id = goal.matchid
GROUP BY game.mdate, game.id, game.team1, game.team2
--	End of Solution

--	Notes
--	1) Need to use CASE WHEN THEN ELSE END to distribute scores to team1 and team2, we don't need to know who is the winner so it's easier
--	2) Use SUM() to summarize the scores for each team
--	3) Most importantly, use LEFT OUTER JOIN from game to goal. There are a few matches that both teams score 0, so the match ids don't even appear in the goal table
--	   A simple JOIN won't include any of these matches so must use LEFT OUTER JOIN, and from game to goal, because only game table includes these matches	
--	End of Notes


/*	Movie Datasets
			movie
id	title	yr	director	budget	gross 
			actor
		id			name 
			casting
movieid		actorid		ord
*/

--	11. Which were the busiest years for 'John Travolta', show the year and the number of movies he made each year for any year in which he made more than 2 movies.

--	Solution
SELECT movie.yr, COUNT(actor.id) AS '# of movies'
FROM actor LEFT JOIN casting on actor.id = casting.actorid
           LEFT JOIN movie on movie.id = casting.movieid
WHERE actor.name = 'John Travolta' 
GROUP BY movie.yr
HAVING COUNT(actor.id) > 2
--	End of Solution

--	Notes
--	Need to know WHERE comes before GROUP BY which comes before HAVING
--	End of Notes