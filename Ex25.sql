﻿--Find the printer makers also producing PCs with the lowest RAM capacity and the highest processor speed of all PCs having the lowest RAM capacity. 
--Result set: maker.

WITH SUBQ AS (
	SELECT
		P.maker,
		PC.model,
		PC.ram,
		PC.speed
	FROM
		Product AS P LEFT JOIN PC ON P.model = PC.model
	WHERE
		P.maker IN (
			SELECT
				P.maker
			FROM
				Printer AS PRT
				LEFT JOIN Product AS P ON PRT.model = P.model
			WHERE P.maker IN (
				SELECT
					P.maker
				FROM
					PC LEFT JOIN Product AS P ON PC.model = P.model
				WHERE PC.code IS NOT NULL
			)
		)
		AND P.type = 'PC'
)
SELECT 
    DISTINCT SUBQ.maker 
FROM 
    SUBQ
WHERE 
    SUBQ.ram = (SELECT MIN(SUBQ.ram) FROM SUBQ)
    AND SUBQ.speed = (
        SELECT 
            MAX(SUBQ.speed) 
        FROM 
            SUBQ
        WHERE SUBQ.ram = (SELECT MIN(SUBQ.ram) FROM SUBQ)
    )
