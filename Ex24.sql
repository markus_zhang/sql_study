﻿--List the models of any type having the highest price of all products present in the database.

WITH TOTAL AS
(
	SELECT
		P.model,
		PC.price
	FROM
		Product AS P
		LEFT JOIN PC ON PC.model = P.model
	WHERE PC.price IS NOT NULL
	UNION
	SELECT
		P.model,
		PRT.price
	FROM
		Product AS P
		LEFT JOIN Printer AS PRT ON PRT.model = P.model
	WHERE PRT.price IS NOT NULL
	UNION
	SELECT
		P.model,
		LAP.price
	FROM
		Product AS P
		LEFT JOIN Laptop AS LAP ON LAP.model = P.model
	WHERE LAP.price IS NOT NULL
)
SELECT
    TOTAL.model
FROM 
    TOTAL
WHERE TOTAL.price = (SELECT MAX(TOTAL.price) FROM TOTAL);
