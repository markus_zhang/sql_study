﻿/*
Find the makers producing at least three distinct models of PCs.
Result set: maker, number of PC models.
*/


SELECT
    P.maker,
    COUNT(DISTINCT P.model) AS Count_Model
FROM
    Product P
WHERE
    P.type = 'PC'
GROUP BY P.maker
HAVING COUNT(DISTINCT P.model) >= 3
