﻿--Data:
--Two tables:
--table1 is one column of ids
--a1, a2, a3, etc...
--table 2 has two columns
--col1 is also a column of ids
--col2 has exactly two items for one id in col1 (see data for example)

CREATE TABLE table1 (
    col1 varchar(2)
);

CREATE TABLE table2 (
    col1 varchar(2),
    col2 varchar(2)
);

INSERT INTO table1 (col1)
VALUES ('a1'),
('a2');

INSERT INTO table2 (col1, col2)
VALUES ('a1', 't1'),
('a1', 't2'),
('a2', 't3'),
('a2', 't4'),
('a3', 't5'),
('a3', 't6');

--Requirements:
--I'd like to have the result as three columns:
--col1	col3_1	col3_2
--a1	t1		t2
--a2	t3		t4
--So a3 should NOT be in the list

--Solution

SELECT col1, col3_1, col3_2 FROM (
  SELECT
    table2.col1, table2.col2,
    CONCAT('col3_', ROW_NUMBER() OVER (PARTITION BY table2.col1 ORDER BY (SELECT 0))) AS col3
  FROM
    table1 INNER JOIN table2 ON table2.col1 = table1.col1
) src
PIVOT (
  MAX(col2)
  FOR col3 IN ([col3_1], [col3_2])
) piv;