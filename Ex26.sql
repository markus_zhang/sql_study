﻿--Find out the average price of PCs and laptops produced by maker A.
--Result set: one overall average price for all items.

SELECT
    AVG(price)
FROM (
	SELECT 
		code, P.model, speed, ram, hd, price 
	FROM 
		Product AS P 
		LEFT JOIN PC ON P.model = PC.model
	WHERE 
		P.maker = 'A' AND P.type = 'PC'
	UNION
	SELECT 
		code, P.model, speed, ram, hd, price FROM Product AS P 
		LEFT JOIN Laptop AS L ON P.model = L.model
	WHERE 
		P.maker = 'A' AND P.type = 'Laptop'
) AS SUBQ

--I have no idea why but this one doesn't work, probably because we should use Type as a filter instead of some NOT NULL
SELECT
    *
FROM (
	SELECT
		DISTINCT maker, P.model, price
	FROM
		Product AS P 
		LEFT JOIN PC ON P.model = PC.model
	WHERE
		PC.price IS NOT NULL AND
		P.maker = 'A'
	UNION
	SELECT
		DISTINCT maker, P.model, price
	FROM
		Product AS P 
		LEFT JOIN Laptop AS LAP ON P.model = LAP.model
	WHERE
		LAP.price IS NOT NULL AND
		P.maker = 'A'
) AS TOTAL