﻿/*
For each ship from the Ships table, determine the name of the earliest battle from the Battles table it could have participated in after being launched. 
If the year the ship has been launched is unknown use the latest battle. 
If no battles occurred after the ship was launched, display NULL instead of the battle name.
It’s assumed a ship can participate in any battle fought in the year of its launching.
Result set: name of the ship, year it was launched, name of battle.
http://www.sql-ex.ru/exercises/index.php?act=learn&LN=75#resPlace
*/


SELECT
    SB.shipname,
    SB.launched,
    SB.battlename
FROM
(
	SELECT 
		S.name AS shipname,
		S.launched,
		--The CASE WHEN THEN ELSE END is to accomodate the situation that launched year is UNKNOWN
		--If it's UNKNOWN then pick the most recent battle from Battles
		CASE WHEN S.launched IS NOT NULL THEN B.name ELSE (
			SELECT
				 TOP 1 B.name
			FROM
				 Battles AS B
			ORDER BY B.[date] DESC
		) END AS battlename,
		B.[date],
		--Use windows function to grab the first battle since launched
		ROW_NUMBER() OVER(PARTITION BY S.name ORDER BY B.[date] ASC) AS rownum
	FROM 
		Ships AS S
		LEFT JOIN Battles AS B ON YEAR(B.[date]) >= S.launched
) AS SB
WHERE SB.rownum = 1