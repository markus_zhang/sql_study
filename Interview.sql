﻿--Q1. Is it possible for a LEFT OUTER JOIN to return less rows than its left hand side table? Please explain

--A1. Yes it is possible. When user indicates a condition in WHERE clause, a LEFT OUTER JOIN turns into an INNER JOIN

SELECT
	*
FROM
	left_table AS lt
	LEFT OUTER JOIN right_table AS rt ON lt.id = rt.id
WHERE
	lt.stamp >= '2018-06-22'

--To retain OUTER JOIN, move the condition to ON clause

SELECT
	*
FROM
	left_table AS lt
	LEFT OUTER JOIN right_table AS rt 
		ON 
			lt.id = rt.id
			AND lt.stamp >= '2018-06-22'

--Q2. Please write the returns of the following queries

--1) NOT IN and NULL
SET ANSI_NULLS OFF;

SELECT
	'True'
WHERE
	3 in (1, 2, NULL)

--2) 