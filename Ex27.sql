﻿--Find out the average hard disk drive capacity of PCs produced by makers who also manufacture printers.
--Result set: maker, average HDD capacity.


SELECT
    maker,
    AVG(hd)
FROM
    Product AS P LEFT JOIN PC ON P.model = PC.model
WHERE
    P.type = 'PC'
    AND P.maker IN (
        SELECT
            P.maker
        FROM
            Product AS P LEFT JOIN Printer AS PRT ON P.model = PRT.model
        WHERE P.type = 'Printer'
    )
GROUP BY maker