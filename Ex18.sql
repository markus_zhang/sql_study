﻿/*
Find the makers of the cheapest color printers.
Result set: maker, price.
*/


SELECT 
    DISTINCT Pro.maker,
    P.price
FROM 
    Printer AS P
    LEFT JOIN Product AS Pro ON Pro.model = P.model
WHERE 
    P.color = 'y'
    AND P.price = (
        SELECT
            MIN(P.price)
        FROM
            Printer AS P
        WHERE P.color = 'y'
    )