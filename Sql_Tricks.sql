﻿--0x01. Generate a series from 1 to 100
SELECT
	TOP 100
	ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS RN
FROM master.dbo.spt_values ORDER BY RN
;

--0x02. Generate a series (another way)
CREATE FUNCTION dbo.ufnGenerateIntSeries
(
	@min INT,
	@max INT
)
RETURNS @int_table TABLE
(
	Number INT
)
AS
BEGIN
	WHILE @min <= @max
	BEGIN
		INSERT INTO @int_table
		SELECT @min
		SET @min = @min + 1
	END
	RETURN;
END
;
--How to use

SELECT
	*
FROM
	dbo.ufnGenerateIntSeries(100, 110) AS FuncGenerateSeries
;

--0x03:	Generate a series of dates

CREATE FUNCTION dbo.ufnGenerateDateSeries
(
	@basedate DATE,
	@days INT
)
RETURNS @date_table TABLE
(
	DateSeries DATE
)
AS
BEGIN
	WHILE @days >= 0
	BEGIN
		INSERT INTO @date_table
		SELECT DATEADD(day, @days, @basedate)

		SET @days = @days - 1
	END
	RETURN;
END

--How to use

SELECT
  FuncGenerateDaySeries.DateSeries
FROM
  dbo.ufnGenerateDateSeries('2018-01-01', 100) AS FuncGenerateDaySeries
ORDER BY
  FuncGenerateDaySeries.DateSeries ASC
;


--0x04: https://stackoverflow.com/questions/55776083/sql-display-2nd-highest-salary-for-each-dept-if-employee-having-same-salary

--Use the characteristics of ROW_NUMBER(), i.e. unlike RANK(), ROW_NUMBER() assigns a different number for
--each row based on PARTITION BY ORDER BY.
--Solution: http://www.sqlfiddle.com/#!18/f37aa/14

CREATE TABLE salary (
	EMPNO INT,
  	ENAME VARCHAR(10),
  	JOB VARCHAR(15),
    SAL INT,
    DEPTNO INT
);

INSERT INTO salary(EMPNO, ENAME, JOB, SAL, DEPTNO)
VALUES(7698,'BLAKE','MANAGER',2850,30),
(7844,'TURNER','SALESMAN',1500,30),
(7499,'ALLEN','SALESMAN',1600,30),
(7654,'MARTIN','SALESMAN',1250,30),
(7521,'WARD','SALESMAN',1250,30),
(7900,'JAMES','CLERK',950,30),
(7788,'SCOTT','ANALYST',3000,20),
(7566,'JONES','MANAGER',2975,20),
(7369,'SMITH','CLERK',25000,20),
(7876,'ADAMS','CLERK',1100,20),
(7902,'FORD','ANALYST',3000,20),
(7839,'KING','PRESIDENT',5000,10),
(7934,'MILLER','CLERK',1300,10),
(7782,'CLARK','MANAGER',2450,10);

SELECT
	*
FROM (
	SELECT
		s.*,
		ROW_NUMBER() OVER(PARTITION BY S.DEPTNO ORDER BY S.SAL DESC, S.EMPNO ASC) AS RowNum
	FROM salary AS s
) AS subq
WHERE subq.RowNum = 2;


--0x05: https://stackoverflow.com/questions/55813884/pivot-rows-to-columns-with-id-not-repeated/55813936
--fiddle: https://dbfiddle.uk/?rdbms=sqlserver_2017&fiddle=385f2e28fb7c77dcb4e5603c2f5e3d0b

--Basically you want to pivot the columns but other columns are in very bad formats:

--Rownumber ID     Type             Category Value
--1         100   1-Purchase Order  Address  Cedar Rd
--2                                 City     Beachwood
--3                                 State    Ohio
--4                                 Zip      44122
--5         300   1-Purchase Order  Address  Rockside Rd
--6                                 City     Independence
--7                                 State    Ohio
--8                                 Zip      44131
--9         200   1-Purchase Order  Address  Rockside Rd
--10                                City     Independence
--11                                State    Ohio
--12                                Zip      44131  

--Desired output:
--Rownumber  ID   Type              Address      City        State  Zip
--1         100  1-Purchase Order  Cedar Rd      Beachwood   Ohio   44122
--2         300   1-Purchase Order Rockside Rd  Independence Ohio   44122
--3         200   1-Purchase Order Rockside Rd  Independence Ohio   44122

with t as (
      select *
      from (values (1, 100, '1-Purchase Order', 'Address', 'Cedar Rd'),
                   (2, NULL, NULL, 'City', 'Beachwood'),
                   (3, NULL, NULL, 'State', 'Ohio'),
                   (4, NULL, NULL, 'Zip', '44122'),
                   (5, 300, '1-Purchase Order', 'Address', 'Cedar Rd'),
                   (6, NULL, NULL, 'City', 'Beachwood'),
                   (7, NULL, NULL, 'State', 'Ohio'),
                   (8, NULL, NULL, 'Zip', '44123'),
                   (9, 200, '1-Purchase Order', 'Address', 'Cedar Rd'),
                   (10, NULL, NULL, 'City', 'Beachwood'),
                   (11, NULL, NULL, 'State', 'Ohio'),
                   (12, NULL, NULL, 'Zip', '44124')
           ) v(Rownumber, ID, Type, Category, Value)
     )
select row_number() over (order by min(rownumber)) as rownumber,
      max(id) as id,
      max(type) as type,
      max(case when category = 'Address' then value end) as address,
      max(case when category = 'City' then value end) as city,
      max(case when category = 'State' then value end) as state,
      max(case when category = 'Zip' then value end) as zip
from (select t.*,
             count(id) over (order by rownumber) as grp
      from t
     ) t
group by grp;

--Explanation:
--1. Look at the output of the inner select:

--Rownumber	ID	Type	Category	Value	   grp
--1	100	1-Purchase Order Address	Cedar Rd	1
--2			             City	Beachwood		1
--3			             State	Ohio			1
--4			             Zip	44122			1
--5	300	1-Purchase Order Address	Cedar Rd	2
--6			             City	Beachwood		2
--7			             State	Ohio			2
--8			             Zip	44123			2
--9	200	1-Purchase Order Address	Cedar Rd	3
--10			         City	Beachwood		3
--11			         State	Ohio			3
--12			         Zip	44124			3

--We can see that it assigns a group for each row, which helps the outer query

--2. In the outer query, I'm still not exactly sure about the row_number() without PARTITION BY
--But max(id) and max(type) basically gives you the longest strings (the others are NULL)
--And the rest of the max() are just for pivoting


--0x06. https://dbfiddle.uk/?rdbms=sqlserver_2017&fiddle=e9e580ccf5b14dc37d904f58447fc785
-- https://ddrscott.github.io/blog/2017/what-the-sql-window/

CREATE TABLE sample_moves (
    id int,
    name varchar(20),
    address varchar(30),
    moved_at date
);
INSERT INTO sample_moves 
    VALUES
      (1, 'Alice' , '1 Main St', '2017-01-01'),
      (2, 'Bob'   , '2 Main St', '2017-02-01'),
      (3, 'Cat'   , '2 Main St', '2017-03-01'),
      (4, 'Dan Sr'  , '3 Main St',  '1970-04-01'),
      (5, 'Dan Jr'  , '3 Main St',  '2001-04-01'),
      (6, 'Dan 3rd' , '3 Main St', '2017-04-01');

--Q1: Get the number of roommates of each name:
--The important part is to JOIN with different names

SELECT
  subq.name,
  ISNULL(COUNT(subq.address), 0) AS count_roommates
FROM (
  SELECT 
    s.name,
    m.address AS address
  FROM sample_moves AS s
    LEFT JOIN sample_moves AS m ON s.address = m.address AND s.name != m.name
) AS subq
GROUP BY subq.name

--Result:
--name	count_roommates
--Alice			0
--Bob			1
--Cat			1
--Dan 3rd		2
--Dan Jr		2
--Dan Sr		2

--Q2: Who moved first?
--We need to use a windows function to partition by address
--FIRST_VALUE() gives you the first thing in a group, so FIRST_VALUE(name) is a good fit

SELECT
  s.id,
  s.name,
  s.address,
  s.moved_at,
  FIRST_VALUE(s.name) OVER(PARTITION BY s.address ORDER BY s.moved_at ASC) AS moved_id
  
FROM
  sample_moves AS s


--0x07. https://stackoverflow.com/questions/55795224/move-columns-to-rows
--Basically wants to move thrid and fourth column under first and second ones

--Original:
--entity	activity	entity_2	activity_2
--XCL204	LLWC	XCL204	OX_NIT
--XCL205	LLWC	XCL205	OX_NIT
--XCL208	LLWC	XCL208	OX_NIT
--XCL210	LLWC	XCL210	OX_NIT
--XCL211	LLWC	XCL211	OX_NIT

--Result:
--entity	activity
--XCL204	LLWC
--XCL205	LLWC
--XCL208	LLWC
--XCL210	LLWC
--XCL211	LLWC
--XCL204	OX_NIT
--XCL205	OX_NIT
--XCL208	OX_NIT
--XCL210	OX_NIT
--XCL211	OX_NIT

--Solution: You can simply solve this with UNION:
-- https://dbfiddle.uk/?rdbms=sqlserver_2017&fiddle=f86eb67d6fcd4c863aff58e85f900a88

with t as (
      select *
      from (values ('XCL204', 'LLWC', 'XCL204', 'OX_NIT'),
                   ('XCL205', 'LLWC', 'XCL205', 'OX_NIT'),
                   ('XCL208', 'LLWC', 'XCL208', 'OX_NIT'),
                   ('XCL210', 'LLWC', 'XCL210', 'OX_NIT'),
                   ('XCL211', 'LLWC', 'XCL211', 'OX_NIT')
           ) v(entity, activity, entity_2, activity_2)
     )

select t.entity, t.activity from t
UNION
select t.entity_2, t.activity_2 from t


--0x08: https://stackoverflow.com/questions/55794261/sql-group-on-occurence-in-x-or-y
--Original:

--Sender   Recipient   Amount    Date
----------------------------------------------------
--Jack     Bob         52        2019-04-21 11:06:32
--Bob      Jack        12        2019-03-29 12:08:11
--Bob      Jill        50        2019-04-19 24:50:26
--Jill     Bob         90        2019-03-20 16:34:35
--Jill     Jack        81        2019-03-25 12:26:54
--Bob      Jenny       53        2019-04-20 09:07:02
--Jack     Jenny       5         2019-03-29 06:15:35

--Desired result: Basically count the number of transactions of each person (could be both Sender and Recipient)
--Plus we want the time of first transaction plus last one, again should consider the person as Sender and Recipient:

--Sender   NUM_TX   First_active               last_active    
--------------------------------------------------------------------
--Jack     4        2019-03-25 12:26:54        2019-04-21 11:06:32
--Bob      5        xxxx-xx-xx xx:xx:xx        xxxx-xx-xx xx:xx:xx
--Jill     3        xxxx-xx-xx xx:xx:xx        xxxx-xx-xx xx:xx:xx
--Jenny    2        xxxx-xx-xx xx:xx:xx        xxxx-xx-xx xx:xx:xx

--Solution:

--We can use UNION to put the Recipient in Sender column and GROUP BY from there:

with t as (
      select *
      from (values ('Jack', 'Bob', 52, '2019-04-21 11:06:32'),
                   ('Bob',      'Jack',        12,        '2019-03-29 12:08:11'),
                   ('Bob',      'Jill',        50,        '2019-04-19 24:50:26'),
                   ('Jill',     'Bob',         90,        '2019-03-20 16:34:35'),
                   ('Jill',     'Jack',        81,        '2019-03-25 12:26:54'),
                   ('Bob',      'Jenny',       53,        '2019-04-20 09:07:02'),
                   ('Jack',     'Jenny',       5,         '2019-03-29 06:15:35')
           ) v(Sender,   Recipient,   Amount,    Date)
     )


SELECT
    subq.Sender,
    COUNT(*) AS NUM_TX,
    MIN(subq.Date) AS First_active,
    MAX(subq.Date) AS Last_active
FROM (
    SELECT t.Sender, t.Amount, t.Date FROM t
    UNION
    SELECT t.Recipient, t.Amount, t.Date FROM t
) AS subq
GROUP BY subq.Sender



--0x09: https://stackoverflow.com/questions/55845584/how-pivot-columns-in-sql-server
--How to pivot the count of a column

--Original:
--Type  |  Value
-----------------
--  A      String
--  A      Int
--  B      String
--  B      String
--  C      Double
--  C      Float

--Desired Output:
--Type  |  String  |  Int  | Double  |  Float
----------------------------------------------
--  A        1         1        0          0
--  B        2         0        0          0 
--  C        0         0        1          1

--Solution:
--Just a simple PIVOT but I'm not familiar with it so I put it here, actually there is no trick about this one:
-- https://dbfiddle.uk/?rdbms=sqlserver_2017&fiddle=b440cfc6b1993e6ab868726bbe83ec4e

with t as (
      select *
      from (values ('A', 'String'),
                   ('A', 'Int'),
                   ('B', 'String'),
                   ('B', 'String'),
                   ('C', 'Double'),
                   ('C', 'Float')
           ) v(type, value)
)


SELECT
    [type], [Double], [Float], [Int], [String]
FROM (
    SELECT
        t.type,
        t.value
    FROM
        t
) AS subq
PIVOT (
    COUNT(subq.value)
    FOR subq.value in ([Double], [Float], [Int], [String])
) AS pt



--0x0a: https://stackoverflow.com/questions/55866622/sql-is-there-a-way-to-select-rows-that-not-similar-by-same-columns

--Original:

--id |AR        |HB        |ENG
-----+----------+----------+-----------
--1  |valueAR   |valueHB   |valueENG
-----+----------+----------+-----------
--2  |TamraAR   |TamraHB   |TamraENG
-----+----------+----------+-----------
--3  |TamraAR   |Tamra2HB  |TamraENG
-----+----------+----------+-----------
--4  |KabulAR   |KabulHB   |KabulENG
-----+----------+----------+-----------
--6  |KabulAR   |KabulHB   |KabulENG
-----+----------+----------+-----------
--7  |KabulAR   |KabulHB   |KabulENG
-----+----------+----------+-----------
--8  |Azor      |Azor      |Azor      
-----+----------+----------+-----------
--9  |Azor      |Azor      |Azor      

--We want all records that have same AR and ENG but different HB, for example row 2 and 3 should be shown

--Result:

--id |AR        |HB        |ENG
-----+----------+----------+-----------
--2  |TamraAR   |TamraHB   |TamraENG
-----+----------+----------+-----------
--3  |TamraAR   |Tamra2HB  |TamraENG
-----+----------+----------+-----------

--Solution: Use a LEFT JOIN with multiple conditions:

CREATE TABLE temp0x0a (
    id int,
    AR varchar(15),
    HB varchar(15),
    ENG varchar(15)
);

INSERT INTO temp0x0a (id, AR, HB, ENG)
VALUES 
    (1, 'valueAR', 'valueHB', 'valueENG'),
    (2, 'TamraAR', 'TamraHB', 'TamraENG'),
    (3, 'TamraAR', 'Tamra2HB', 'TamraENG'),
    (4, 'KabulAR', 'KabulHB', 'KabulENG'),
    (6, 'KabulAR', 'KabulHB', 'KabulENG'),
    (7, 'KabulAR', 'KabulHB', 'KabulENG'),
    (8, 'Azor', 'Azor', 'Azor'),
    (9, 'Azor', 'Azor', 'Azor');

SELECT
    T2.*
FROM
    temp0x0a AS T1
    LEFT JOIN temp0x0a AS T2 ON T1.AR = T2.AR AND T1.ENG = T2.ENG AND T1.HB <> T2.HB